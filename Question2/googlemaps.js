// Uses GoogleMaps API v3
// Author: Patrick Khensovan <patrick@patrickkhensovan.com>

// Create the necessary global variables
var map;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();

var clickTime;
var pos;

function initialize() {
  directionsDisplay = new google.maps.DirectionsRenderer();

  var marker;
  var mapOptions = {
    zoom: 17,
    mapTypeId: google.maps.MapTypeId.ROADMAP    
  };

  // Initialize ClickTime's address
  clickTime = "282 2nd Street 4th floor, San Francisco, CA 94105"

  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  // Our info window for the different locations that populate from search
  var infowindow = new google.maps.InfoWindow();
  var service = new google.maps.places.PlacesService(map);

  // Try HTML5 geolocation
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      pos = new google.maps.LatLng(position.coords.latitude,
                                   position.coords.longitude);

      map.setCenter(pos);

      // Sets our marker to our initial location
      marker = new google.maps.Marker({
          position: pos,
          map: map
      });

      directionsDisplay.setMap(map);
      directionsDisplay.setPanel(document.getElementById('directions-panel'));

      var control = document.getElementById('control');
      control.style.display = 'block';
      map.controls[google.maps.ControlPosition.TOP_CENTER].push(control);

      var defaultBounds = map.getBounds();
      map.fitBounds(defaultBounds);
    }, function() {
      handleNoGeolocation(true);
    });
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }

}

function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }

  var options = {
    map: map,
    position: new google.maps.LatLng(60, 105),
    content: content
  };

  var infowindow = new google.maps.InfoWindow(options);
  map.setCenter(options.position);
}

function calcRoute() {
  var start = pos;
  var end = clickTime;
  var selectedMode = document.getElementById('mode').value;
  var request = {
    origin: start,
    destination: end,
    travelMode: google.maps.TravelMode[selectedMode]
  };
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
}

function getCoffeeRoute() {
  var start = pos;
  var end = clickTime;
  var coffeeStop = [];
  var selectedMode = document.getElementById('mode').value;

  // Accounts if we decide not to grab any coffee on the go
  if(document.getElementById('get-coffee').value != "") {
    coffeeStop.push({
      location:document.getElementById('get-coffee').value,
      stopover:true
    });
  } else {
    calcRoute();
  }

  var request = {
    origin: start,
    destination: end,
    waypoints: coffeeStop,
    optimizeWaypoints: true,
    travelMode: google.maps.TravelMode[selectedMode]
  };

  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
      var route = response.routes[0];
      var summaryPanel = document.getElementById('directions-panel');
      summaryPanel.innerHTML = '';
      // For each route, display summary information.
      for (var i = 0; i < route.legs.length; i++) {
        var routeSegment = i + 1;
        summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
        summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
        summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
        summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
      }
    }    
  });
}

google.maps.event.addDomListener(window, 'load', initialize);